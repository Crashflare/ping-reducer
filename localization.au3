#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.12.0
 Author:         Alex Kamuro

 Script Function:
	Translations into various languages.

#ce ----------------------------------------------------------------------------

Global Const $OSLang = @OSLang

Global $LOCAL_status_label = "Status:"
Global $LOCAL_path_installed = "Installed"
Global $LOCAL_path_not_installed = "Not installed"
Global $LOCAL_apply_btn_install = "Install"
Global $LOCAL_apply_btn_remove = "Remove"
Global $LOCAL_apply_btn_restart = " Restart"
Global $LOCAL_program_details = " " & @CRLF & "Copyright � Alex Kamuro 2015"
Global $LOCAL_welcome_text = "Hi!" & @CRLF & @CRLF & "This program is useful for gamers. It modifies your network configuration for reduced latency in online games." & @CRLF
Global $LOCAL_welcome_install = @CRLF & "To apply the new settings click the Install button below. A system restart is required after installation or removal."
GlobaL $LOCAL_welcome_no_admin = @CRLF & "To apply the new settings require administrator privileges. Click the Restart button below to run this program as an administrator."
Global $LOCAL_restart_link = "A system restart is required"
Global $LOCAL_restart_link_tip = "Click to restart your computer"
Global $LOCAL_msgbox_details_title = "About the program"
Global $LOCAL_msgbox_restart_title = "Restart"
Global $LOCAL_msgbox_restart_text = "Restart your computer now?"

; Russin
If $OSLang == "0419" Then
   $LOCAL_status_label = "������:"
   $LOCAL_path_installed = "�����������"
   $LOCAL_path_not_installed = "�� �����������"
   $LOCAL_apply_btn_install = "����������"
   $LOCAL_apply_btn_remove = "�������"
   $LOCAL_apply_btn_restart = " �������������"
   $LOCAL_welcome_text = "������!" & @CRLF & @CRLF & "������ ��������� ������� ��� ��������. ��� ������������ ���� ������� ��������� ��� ����� ������� �������� � ������ �����." & @CRLF
   $LOCAL_welcome_install = @CRLF & "��� ���� ����� ��������� ����� ��������� ������� ������ ����������. ����� ���������/�������� ���������� ����������� ������������ ����������."
   $LOCAL_welcome_no_admin = @CRLF & "��� ���������� ����� ���������� ��������� ����� ��������������. ������� ������ ������������� ����� ��������� ��������� � ������� ��������������."
   $LOCAL_restart_link = "��� ���������� ���������� ��������� ������������"
   $LOCAL_restart_link_tip = "�������� ��� ������������ ����������"
   $LOCAL_msgbox_details_title = "� ���������"
   $LOCAL_msgbox_restart_title = "������������"
   $LOCAL_msgbox_restart_text = "��������� ������������ ���������� ������?"
EndIf

