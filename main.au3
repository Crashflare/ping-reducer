#cs ----------------------------------------------------------------------------

 Author:         Alex Kamuro

 Script Function:
	Main script.

#ce ----------------------------------------------------------------------------

#NoTrayIcon
#Region
#AutoIt3Wrapper_Icon=icons\main.ico
#AutoIt3Wrapper_UseUpx=n
#AutoIt3Wrapper_Res_Description=
#AutoIt3Wrapper_Res_Field=ProductName|Kamuro Ping Reducer
#AutoIt3Wrapper_Res_ProductVersion=1.0
#AutoIt3Wrapper_Res_Fileversion=1.0.0.130
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=y
#AutoIt3Wrapper_Res_LegalCopyright=Alex Kamuro
#AutoIt3Wrapper_Run_After=tools\ResHacker.exe -add "main.exe", "main.exe", "icons\info.ico", ICONGROUP, 9999, 0
#AutoIt3Wrapper_Run_After=tools\mpress.exe -q -r -s main.exe
#EndRegion

#include <GUIConstants.au3>
#include <GuiButton.au3>
#include <ColorConstants.au3>
#include <Constants.au3>
#include "include\GUIHyperLink.au3"
#include "localization.au3"
Opt("MustDeclareVars", 1)

Global $INSTALLED = False
Global $WINDOW_WAS_RESIZED = False
Global $HKLM = "HKLM"
If Not StringInStr(@OSArch, "X86") Then $HKLM = "HKLM64"
Global $QUIET = False

CommandLineCheck()

If Not $QUIET Then

	;----------- Interface --------------
	Local $main_window = GUICreate("Kamuro Ping Reducer v" & FileGetVersion(@AutoItExe),400,205)
		Local $ctrl_welcome_text = GUICtrlCreateLabel($LOCAL_welcome_text & $LOCAL_welcome_install, 20, 20, 360, 110)
		GUICtrlSetResizing (-1, $GUI_DOCKALL)

		GUICtrlCreateLabel($LOCAL_status_label, 20, 160, 50, 30)
		GUICtrlSetFont(-1, 10)
		GUICtrlSetResizing (-1, $GUI_DOCKALL)
		Local $ctrl_installed = GUICtrlCreateLabel("", 70, 160, 150, 30)
		GUICtrlSetResizing ($ctrl_installed, $GUI_DOCKALL)
		GUICtrlSetFont($ctrl_installed, 10)

		Local $ctrl_apply = GUICtrlCreateButton("", 230, 150, 130, 35, $BS_DEFPUSHBUTTON)
		GUICtrlSetResizing ($ctrl_apply, $GUI_DOCKALL)
		GUICtrlSetOnEvent($ctrl_apply, "Apply")

		GUICtrlCreateGraphic(5,207,390,1,$SS_GRAYRECT)
		GUICtrlSetResizing (-1, $GUI_DOCKALL)
		Local $ctrl_reboot_icon = GUICtrlCreateIcon(@AutoItExe,"9999" , 20, 213, 32, 32)
		GUICtrlSetResizing ($ctrl_reboot_icon, $GUI_DOCKALL)
		Local $ctrl_reboot = _GUICtrlHyperLink_Create($LOCAL_restart_link, 80, 222, 300, 20, 0x0000FF, 0x551A8B, _
		1, 'Reboot()', $LOCAL_restart_link_tip, $main_window)
		GUICtrlSetResizing (-1, $GUI_DOCKALL)


	Opt("GUIOnEventMode", 1)
	GUISetOnEvent($GUI_EVENT_CLOSE, "SpecialEvents")
	GUISetOnEvent($GUI_EVENT_MINIMIZE, "SpecialEvents")
	GUISetOnEvent($GUI_EVENT_RESTORE, "SpecialEvents")


	GUISetState(@SW_SHOW, $main_window)
EndIf

Func SpecialEvents()
	Select
		Case @GUI_CTRLID = $GUI_EVENT_CLOSE
			Exit
		Case @GUI_CTRLID = $GUI_EVENT_MINIMIZE

		Case @GUI_CTRLID = $GUI_EVENT_RESTORE

	EndSelect
EndFunc
;------------------------------------


Update()
If Not $QUIET Then WinActivate($main_window)

While 1
   Sleep(10)
Wend


; interface updating
Func Update()
	$INSTALLED = True
	Local $i, $path

	;---------- TcpAckFrequency ---------
	$i = 1
	$path = $HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces"
	While 1
		Local $dir = RegEnumKey($path, $i)
		If @error <> 0 then ExitLoop
		If $dir <> "" Then
			Local $value = RegRead($path & "\" & $dir, "TcpAckFrequency")
			If @error <> 0 Then $INSTALLED = False
			If $value <> "1" Then $INSTALLED = False
		EndIf
		$i += 1
	WEnd

	;---------- SackOpts ---------
	$path = $HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters"
	Local $value = RegRead($path, "SackOpts")
	If @error <> 0 Then $INSTALLED = False
	If $value <> "1" Then $INSTALLED = False


	If $INSTALLED Then
		If IsAdmin() Then GUICtrlSetData($ctrl_apply, $LOCAL_apply_btn_remove)
		GUICtrlSetData($ctrl_installed, $LOCAL_path_installed)
		GUICtrlSetColor($ctrl_installed, $COLOR_GREEN)
	Else
		If IsAdmin() Then GUICtrlSetData($ctrl_apply, $LOCAL_apply_btn_install)
		GUICtrlSetData($ctrl_installed, $LOCAL_path_not_installed)
		GUICtrlSetColor($ctrl_installed, $COLOR_RED)
	EndIf

	If Not IsAdmin() Then
		GUICtrlSetData($ctrl_apply, $LOCAL_apply_btn_restart)
		_GUICtrlButton_SetShield($ctrl_apply)
		GUICtrlSetData($ctrl_welcome_text, $LOCAL_welcome_text & $LOCAL_welcome_no_admin)
	EndIf

EndFunc		;==>Update

; "Install" button handler
Func Apply()
	GUICtrlSetState($ctrl_apply,$GUI_DISABLE)
	;WindowResize()
	;Return
	If Not IsAdmin() Then
		Local $result
		If @Compiled Then
			$result = ShellExecute(@ScriptFullPath, "", @WorkingDir, "runas")
		Else
			$result = ShellExecute(@AutoItExe, '/AutoIt3ExecuteScript "' & @ScriptFullPath & '"', @WorkingDir, "runas")
		EndIf
		If $result Then
			Exit
		Else
			GUICtrlSetState($ctrl_apply,$GUI_ENABLE)
			Return
		EndIf
	EndIf

	If $INSTALLED Then
		Remove()
	Else
		Install()
	EndIf

	Local $old_installed = $INSTALLED
	Update()

	GUICtrlSetState($ctrl_apply,$GUI_ENABLE)
	If ($old_installed <> $INSTALLED) Then WindowResize()
EndFunc		;==>Apply

Func Install()
	If Not IsAdmin() Then Return
	;---------- TcpAckFrequency ---------
	Local $i = 1
	While 1
		Local $dir = RegEnumKey($HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces", $i)
		If @error <> 0 then ExitLoop
		If $dir <> "" Then
			Local $path = $HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces" & "\" & $dir
			Local $reg = RegWrite($path, "TcpAckFrequency", "REG_DWORD", "1")
		EndIf
		$i += 1
	WEnd

	Local $path = $HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters"
	Local $reg = RegWrite($path, "SackOpts", "REG_DWORD", "1")
EndFunc		;==>Install

Func Remove()
	If Not IsAdmin() Then Return
	;---------- TcpAckFrequency ---------
	Local $i = 1
	While 1
		Local $dir = RegEnumKey($HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces", $i)
		If @error <> 0 then ExitLoop
		If $dir <> "" Then
			Local $path = $HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces" & "\" & $dir
			RegDelete($path, "TcpAckFrequency")
		EndIf
		$i=$i+1
	WEnd

	;---------- SackOpts ---------
	Local $path = $HKLM & "\SYSTEM\CurrentControlSet\services\Tcpip\Parameters"
	RegDelete($path, "SackOpts")
EndFunc		;==>Remove

Func Reboot()
    Local $result = MsgBox(4 + 32, $LOCAL_msgbox_restart_title, $LOCAL_msgbox_restart_text, 0, $main_window)
	If $result == 6 Then	;if Yes
		Shutdown(2)
	EndIf
EndFunc		;==>Reboot

Func WindowResize()
	If $WINDOW_WAS_RESIZED Then Return;

	Local $position = WinGetPos($main_window)
	Local $width = $position[2]
	Local $height = $position[3]
	Local $new_height = $height + 50
	Local $step = 3
	While $height < $new_height
		$height = $height + $step
		if ($height > $new_height) Then $height = $new_height
		WinMove($main_window, "", Default, Default, $width, $height)
		Sleep(1)
	WEnd
	$WINDOW_WAS_RESIZED = True
EndFunc		;==>WindowResize

Func CommandLineCheck()
    If $CmdLine[0] Then
        Switch $CmdLine[1]
			Case '-u', '/u', '-uninstall', '/uninstall'
				$QUIET = True
                Remove()
				Exit
            Case '-i', '/i', '-install', '/install'
				$QUIET = True
                Install()
				Exit
            Case Else
				Exit
        EndSwitch
    EndIf
EndFunc		;==>CommandLineCheck
